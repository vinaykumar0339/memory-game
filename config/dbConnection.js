const { Sequelize } = require('sequelize');
const { mysql } = require('./config');

const sequelize = new Sequelize(mysql.db, mysql.user, mysql.pass, {
    host: mysql.host,
    dialect: mysql.dialect,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Player = require("../models/Player")(sequelize, Sequelize);
db.Score = require("../models/Score")(sequelize, Sequelize);

db.Player.hasMany(db.Score, { as: "scores" });
db.Score.belongsTo(db.Player, {
    foreignKey: "playerId",
    as: "player",
});

module.exports = db;


