if (process.env.NODE_ENV != 'production') {
    const dotenv = require('dotenv');
    dotenv.config();
}

module.exports = {
    mysql: {
        host: process.env.DB_HOST || 'localhost',
        user: process.env.DB_USER || 'vinay',
        pass: process.env.DB_PASS || 'Vinayhema@0339',
        db: process.env.DB_DATABASE || 'memory_game',
        dialect: 'mysql'
    },
    mail: {
        apiKey: process.env.MAILGUN_API_KEY,
        domain: process.env.MAILGUN_DOMAIN
    },
    port: process.env.PORT || 3000,
}