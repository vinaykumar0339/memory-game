const express = require('express');
const db = require('./config/dbConnection');
const sendMail = require('./sendEmail');

const app = express();

app.use(express.static('public'));

app.use(express.json());

app.use(express.urlencoded({ extended: false }));

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

app.get('/top-five', async (req, res) => {

    try {
        const scores = await db.Score.findAll({
            group: ['playerId'],
            attributes: [
                [db.Sequelize.fn('MAX', db.Sequelize.col('score.createdAt')), 'createdAt'],
                [db.Sequelize.fn('MIN', db.Sequelize.col('best_score')), 'score']
            ],
            order: [
                [db.Sequelize.col('score'), 'ASC']
            ],
            limit: 5,
            include: ['player']
        })

        res.status(200).send(scores)
    } catch (e) {
        console.log(e);
        res.status(500).json({
            err: e
        });
        res.end();
    }

})

app.post('/send-score', async (req, res) => {

    if (req.body.name === undefined || req.body.email === undefined) {
        return res.status(400).json({
            msg: 'please provide name and email fields',
        }).end();
    }

    try {
        const player = await db.Player.findOne({
            where: {
                name: req.body.name,
                email: req.body.email
            }
        })

        if (!player) {
            const newPlayer = await db.Player.create({
                name: req.body.name,
                email: req.body.email
            })

            await db.Score.create({
                playerId: newPlayer.id,
                best_score: req.body.score,
            })

            sendMail(req.body.name, req.body.email, req.body.score, (err, data) => {
                if (err) {
                    res.status(500).json({
                        msg: 'internal server error please try later'
                    });
                    res.end();
                } else {
                    res.status(201).json({
                        msg: `email send to ${req.body.email} successfully`
                    });
                    res.end();
                }
            });



        } else {
            await db.Score.create({
                playerId: player.id,
                best_score: req.body.score,
            })

            sendMail(req.body.name, req.body.email, req.body.score, (err, data) => {
                if (err) {
                    res.status(500).json({
                        msg: 'internal server error please try later'
                    });
                    res.end();
                } else {
                    res.status(201).json({
                        msg: `email send to ${req.body.email} successfully`
                    });
                    res.end();
                }
            });
        }

    } catch (e) {
        console.log(e.message);
        res.status(500).json({
            err: e.message
        });
        res.end();
    }

});

const { port } = require('./config/config');
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
})