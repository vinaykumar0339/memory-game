module.exports = (sequelize, Sequelize) => {
    const Player = sequelize.define("player", {
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
    });

    return Player;
};