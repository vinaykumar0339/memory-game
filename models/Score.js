module.exports = (sequelize, Sequelize) => {
    const Score = sequelize.define("score", {
        best_score: {
            type: Sequelize.INTEGER
        },
    });

    return Score;
};