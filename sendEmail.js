const nodemailer = require("nodemailer");
var mg = require('nodemailer-mailgun-transport');
const { mail } = require('./config/config');

function sendMail(name, email, score, cb) {


    // create reusable transporter object using the default SMTP transport

    let auth = {
        auth: {
            api_key: mail.apiKey,
            domain: mail.domain
        }
    }
    const transporter = nodemailer.createTransport(mg(auth));

    // send mail with defined transport object
    transporter.sendMail({
        from: '"memory-game" noreply@memory-game.com', // sender address
        to: `${email}`, // list of receivers
        subject: "Your Memory Game Score", // Subject line
        // text: "Hello world?", // plain text body
        html: `<p>Hello, ${name}</P>
                <p>You got the best score in memory game: ${score}</p>`, // html body
    }, (err, data) => {
        if(err) {
            cb(err, null);
        }else {
            cb(null, data);
        }
    });
}

module.exports = sendMail;