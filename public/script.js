const gameContainer = document.getElementById("game");
const start = document.querySelector('.start');
const reStart = document.querySelector('.re-start');
const gameOver = document.getElementById('gameover');
const playAgain = document.querySelector('.playagain');
const form = document.getElementById('form');

const IMAGES = [
  './gifs/1.gif',
  './gifs/2.gif',
  './gifs/3.gif',
  './gifs/4.gif',
  './gifs/5.gif',
  './gifs/6.gif',
  './gifs/7.gif',
  './gifs/8.gif',
  './gifs/9.gif',
  './gifs/10.gif',
  './gifs/11.gif',
  './gifs/12.gif',
  './gifs/1.gif',
  './gifs/2.gif',
  './gifs/3.gif',
  './gifs/4.gif',
  './gifs/5.gif',
  './gifs/6.gif',
  './gifs/7.gif',
  './gifs/8.gif',
  './gifs/9.gif',
  './gifs/10.gif',
  './gifs/11.gif',
  './gifs/12.gif',
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
let loadingBackGifs = 0;
function createDivsForImages(imageArray) {
  for (let image of imageArray) {
    // create a new div
    const newDiv = document.createElement("div");
    newDiv.classList.add('flipCard');

    // image card
    const newImg1 = document.createElement('img');
    newImg1.src = './images/image-front.png';
    newImg1.classList.add('image-front');
    newDiv.appendChild(newImg1);


    // gif card
    const newImg2 = document.createElement('img');
    newImg2.src = image;
    newImg2.classList.add('image-back');
    newDiv.appendChild(newImg2);



    // call a function handleCardClick when a div is clicked on
    newImg1.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    newImg2.addEventListener('load', (e) => {
      loadingBackGifs++;
      if (loadingBackGifs === 12) {
        const loadingPlaceholder = document.querySelector('.loading');
        loadingPlaceholder.classList.add('hide');
        gameContainer.classList.remove('hide');
      }
    })
    gameContainer.appendChild(newDiv);
  }
}

let isFlipped = false;
let winningCount = 0;
let lockBoard = false;
let firstCard = null;
let secondCard = null;
let scoreCount = 0;

// TODO: Implement this function!
function handleCardClick(event) {

  reStart.classList.remove('hide');
  // check board is locked or not
  if (lockBoard) return;

  // check if flip class is added or not
  if (event.target.parentElement.classList.contains("flip")) return;

  // adding flip class to the target element
  event.target.parentElement.classList.add('flip');

  if (!isFlipped) {
    firstCard = event;
    isFlipped = true;
  } else {
    secondCard = event;
    compareCards();
  }

  scoreCount++;
  updateScore(scoreCount);

  checkWin();


}

// compare cards
function compareCards() {
  firstImg = firstCard.target.nextSibling.src;
  secondImg = secondCard.target.nextSibling.src;

  if (firstImg !== secondImg) {
    lockBoard = true;
    setTimeout(resetCards, 1000, firstCard, secondCard);
  } else {
    winningCount++;
  }

  isFlipped = false;
}


// checking for win
function checkWin() {
  if (winningCount === 12) {
    gameOverPopUp();
  }
}

// reset the unmatched cards
function resetCards(firstCard, secondCard) {
  firstCard.target.parentElement.classList.remove('flip');
  secondCard.target.parentElement.classList.remove('flip');
  lockBoard = false;
}

// update score
function updateScore(scoreCount) {
  const scoreDiv = document.querySelector('.counter');
  scoreDiv.innerText = scoreCount;
}

// game over pop up
function gameOverPopUp() {

  const currentScore = document.querySelector('.gameover-currentscore');

  currentScore.innerText = `Current Score: ${scoreCount}`;

  gameOver.classList.remove('hide');
  reStart.classList.add('hide');

  const name = localStorage.getItem('name');
  const email = localStorage.getItem('email');

  if (name !== undefined && email !== undefined) {
    form['name'].value = name;
    form['email'].value = email;
  }

}

// loading the game
function loadingGame() {

  let shuffledImages = shuffle(IMAGES);

  isFlipped = false;
  winningCount = 0;
  lockBoard = false;
  firstCard = null;
  secondCard = null;
  scoreCount = 0;


  createDivsForImages(shuffledImages);

  updateScore(scoreCount);
  gameOver.classList.add('hide');
}

// message fadeout 
function fadeOut(msg) {
  const message = document.querySelector('.message');
  message.innerText = msg;
  let op = 1;
  message.classList.add('display');
  let timer = setInterval(function () {
    if (op <= 0){
      clearInterval(timer);
    }
    message.style.opacity = op;
    op -= 0.01;
  }, 50)
}


// start game
start.addEventListener('click', () => {
  const loadingPlaceholder = document.querySelector('.loading');

  start.classList.add('hide');
  loadingPlaceholder.classList.remove('hide');
  loadingGame();
})

// restart button
reStart.addEventListener('click', () => {
  gameContainer.innerText = '';
  reStart.classList.add('hide');
  loadingGame();
})

// play again
playAgain.addEventListener('click', () => {
  gameContainer.innerText = '';
  reStart.classList.add('hide');
  playAgain.classList.add('hide');
  loadingGame();
});

// send mail 
form.addEventListener('submit', (e) => {

  let errors = false;
  const scoreDiv = document.querySelector('.counter');
  const nameError = document.querySelector('#name-error');
  const emailError = document.querySelector('#email-error');


  const name = form['name'].value;
  const email = form['email'].value;
  const score = parseInt(scoreDiv.innerText);

  const emailMatch = /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;

  if (name.length === 0) {
    errors = true;
    displayErrors('name', nameError, 'name should not be empty');
  } else {
    hideErrors('name', nameError);
  }

  if (!emailMatch.test(email)) {
    errors = true;
    displayErrors('email', emailError, 'please provide valid email');
  } else {
    hideErrors('email', emailError);
  }

  const data = {
    name,
    email,
    score
  }

  if (!errors) {
    fetch('/send-score', {
      body: JSON.stringify(data),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then((resData) => {

        localStorage.setItem('name', data.name);
        localStorage.setItem('email', data.email);

        gameOver.classList.add('hide');
        playAgain.classList.remove('hide');

        const list = document.querySelector('.list');
        list.innerHTML = '';

        getTopFiveScore();
        // reset the form
        e.target.reset();
        form['name'].classList.remove('success');
        form['email'].classList.remove('success');
        
        fadeOut(resData.msg);

        gameContainer.innerText = '';
        loadingGame();

        console.log(resData);
      }).catch(console.log)
  }

  e.preventDefault();

})

// set the errors
function displayErrors(fieldName, errorTag, errorMsg) {
  form[fieldName].classList.remove('success');
  form[fieldName].classList.add('error');

  errorTag.classList.add('error');
  errorTag.innerText = errorMsg;
}

// remove the errors
function hideErrors(fieldName, errorTag) {
  form[fieldName].classList.remove('error');
  form[fieldName].classList.add('success');
  errorTag.classList.remove('error')
}

// get top 5 scores
function getTopFiveScore() {

  const list = document.querySelector('.list');
  const newLi1 = document.createElement('li');
  const newLi2 = document.createElement('li');
  const newLi3 = document.createElement('li');

  newLi1.innerHTML = '<small class="head">name</small>'
  newLi2.innerHTML = '<small class="head">score</small>'
  newLi3.innerHTML = '<small class="head">date</small>'

  fetch('/top-five')
    .then(res => res.json())
    .then((scores) => {
      scores.forEach(score => {

        const small1 = document.createElement('small');
        const small2 = document.createElement('small');
        const small3 = document.createElement('small');

        small1.innerText = score.player.name;
        // small1.classList.add('text');
        small2.innerText = score.score;
        small2.classList.add('score');
        small3.innerText = score.createdAt.slice(0, score.createdAt.indexOf('T'));

        newLi1.append(small1);
        newLi2.append(small2);
        newLi3.append(small3);
      })
      list.append(newLi1, newLi2, newLi3);
    }).catch(console.log)
}

getTopFiveScore();


